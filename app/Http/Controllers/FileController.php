<?php


namespace App\Http\Controllers;

use App\File;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $files = File::all();
        return view('files.index', compact('files'), compact('user'));
    }

    public function create()
    {
        return view('files.upload');
    }

    public function created()
    {
        $user = Auth::user();
        $files = File::with(function ($query) use ($user) {
            $query->where('userid', $user->id);
        });
        return view('file.created', compact($files));
    }

    public function store(Request $request)
    {
        $request->validate([
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $file = $request->all();
        $file['uuid'] = (string)Uuid::generate();
        if ($request->hasFile('cover')) {
            $file['cover'] = $file['uuid'] . "_" . $request->cover->getClientOriginalName();
            $request->cover->storeAs('files', $file['cover'], 'uploads');
        } else {
            return view('files.notcreated')->with('msg', 'فایلی مشخص نشده است!');
        }
        if (!$request->has('title')) {
            $file['title'] = $request->cover->getClientOriginalName();
        }
        $file['userid'] = Auth::user()->id;
        $file['hasPassword'] = false;
        $createdFile = File::create($file);
        return view('files.created')->with('file', $createdFile);
    }

    public function download($uuid)
    {
        $file = File::where('uuid', $uuid)->firstOrFail();
        $pathToFile = storage_path('app/files/' . $file->cover);
        return response()->download($pathToFile);
    }

    public function show($uuid)
    {
        $file = File::where('uuid', $uuid)->firstOrFail();
        $user = User::where('id', $file->userid)->firstOrFail();
        return view('files.show')->with('file', $file)->with('user', $user);
    }

    public function delete($uuid)
    {
        $file = File::where('uuid', $uuid)->firstOrFail();
        $fileUser = User::where('id', $file->userid)->firstOrFail();
        $thisUser = Auth::user();
        $msg = 'فایل '.$file->title .' پاک شد!';

        if($fileUser->id == $thisUser->id){
            $file->delete();
        }else{
            return view('files.delete')->with('error', 'فایل متعلق به شما نیست!');
        }
        return back()->with('success',$msg);
    }

}