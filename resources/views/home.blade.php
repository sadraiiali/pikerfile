@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 justify-content-center text-center">

                @if ($message = Session::get('success'))

                    <div class="alert alert-success alert-block">

                        <button type="button" class="close" data-dismiss="alert">×</button>

                        <strong class="pr-2">{{ $message }}</strong>

                    </div>

                @endif

                <div class="row">
                    <a href="/files/create" class="btn btn-block btn-outline-light m-3 samim" style="font-size: 1.3rem">فایل
                        جدید ؟</a>
                </div>
                <div class="card">
                    <div class="card-header text-center beshkaste" style="font-size: 3rem">فایل‌های من</div>

                    <div class="card-body samim">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @foreach($files as $file)
                            <div class="card">
                                <div class="row">
                                    <button class="btn btn-dark d-inline-block">
                                        <i class="far fa-file-alt" style="font-size: 1rem;"></i>

                                        {{ $file->title }}
                                    </button>
                                    <a href="/files/{{ $file->uuid }}/delete" class="mr-auto btn btn-outline-danger"><i
                                                class="fas fa-trash"></i></a>

                                    <a href="/files/{{ $file->uuid }}/show" class="btn mr-2 btn-primary"><i
                                                class="far fa-eye"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
