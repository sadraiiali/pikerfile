@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header beshkaste" style="font-size: 3rem">
                        <i class="far fa-file-alt" style="font-size: 1rem; color: red;"></i>
                        فایل
                        ساخته نشد
                    </div>

                    <div class="card-body samim">
                        {{ $msg }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
