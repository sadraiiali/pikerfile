@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header beshkaste" style="font-size: 3rem">
                        <i class="far fa-file-alt" style="font-size: 1rem"></i>
                        فایل
                        <span class="fileName samim">
                            {{ $file->title }}
                        </span>
                        ساخته شد
                    </div>

                    <div class="card-body samim">
                        شما میتوانید با

                        <a href="files/{{ $file->uuid  }}/show">این آدرس</a>
                        به این فایل دسترسی داشته باشید.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
