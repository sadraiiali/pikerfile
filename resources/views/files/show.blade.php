@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header beshkaste" style="font-size: 3rem">
                        <div class="row justify-content-center">
                            <img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" height="150"
                                 width="150" style="position: relative;right: 14px;"/>
                            <i class="fas fa-share" id="shareIcon"></i>
                        </div>
                        <div class="=justify-content-center text-center">
                            <span class="userName samim" style="max-width: 100%">
                                <i class="fas fa-user"></i>
                                {{ $user->name }}
                            </span>
                        </div>
                        <div class="=justify-content-center text-center">
                            <span class="fileName samim" style="max-width: 100%;line-height: 2rem">
                            <i class="far fa-file-alt" style="font-size: 1rem;"></i>
                                {{ $file->title }}
                            </span>
                        </div>

                    </div>

                    <div class="card-body samim text-center">
                        شما میتوانید با

                        <a href="../{{ $file->uuid  }}/download">این آدرس</a>
                        به این فایل دسترسی داشته باشید.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
