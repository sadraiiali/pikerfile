@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header beshkaste text-center" style="font-size: 4rem" >آپلود فایل جدید</div>

                    <div class="card-body samim">

                        <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            عنوان فایل:
                            <br>
                            <input type="text" name="title" class="form-control">

                            <br>

                            فایل:
                            <br>
                            <style>

                                /****** CODE ******/

                                .file-upload{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
                                .file-upload .file-select{display:block;border: 2px solid #dce4ec;border-radius: 4px;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
                                .file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
                                .file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
                                .file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                                .file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                                .file-upload.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                                .file-upload.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                                .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
                                .file-upload .file-select.file-select-disabled{opacity:0.65;}
                                .file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
                                .file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
                                .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
                            </style>

                            <div class="file-upload mt-3 samim" style="direction: ltr">
                                <div class="file-select">
                                    <div class="file-select-button" id="fileName">انتخاب فایل</div>
                                    <div class="file-select-name" id="noFile">هیچ فایلی انتخاب نشده است</div>
                                    <input type="file" name="cover" aria-describedby="fileHelp" id="avatarFile">
                                </div>
                            </div>
                            <script>
                                $('#avatarFile').bind('change', function () {
                                    var filename = $("#avatarFile").val();
                                    if (/^\s*$/.test(filename)) {
                                        $(".file-upload").removeClass('active');
                                        $("#noFile").text("فایلی انتخاب نشده...");
                                    }
                                    else {
                                        $(".file-upload").addClass('active');
                                        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
                                    }
                                });
                            </script>

                            <br>
                            <input type="checkbox" name="hasPassword">
                            پسورد داره؟
                            <br><br>

                            <input type="submit" value="آپلود فایل" class="btn btn-primary">

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection