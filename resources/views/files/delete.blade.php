@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if(isset($error))
                        <div class="card-header beshkaste justify-content-center text-center" style="font-size: 3rem">
                            <i class="fas fa-angry"></i>
                            <h1 style="font-size: 4rem">ای بابا ...</h1>
                        </div>

                        <div class="card-body samim text-center">
                            {{ error }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
