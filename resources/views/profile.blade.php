@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{asset('css/cropper.css')}}">
    <script src="{{assert('js/cropper.js')}}"></script>
    <div class="container samim">
        <div class="row">
            <div class="col-12">
                @if ($message = Session::get('success'))

                    <div class="alert alert-success alert-block">

                        <button type="button" class="close" data-dismiss="alert">×</button>

                        <strong class="pr-2">{{ $message }}</strong>

                    </div>

                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close pl-2" data-dismiss="alert">×</button>
                        <strong>اوپس !</strong> هنگام عوض کردن نمایه کاربری اشکالی رخ داده است.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container pt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-8 card pt-5 pb-5">
                <div class="row justify-content-center">
                    <style>
                        .name{
                            font-size: 3rem;
                            text-align: center;
                            display: block;
                            margin-bottom: 28px;
                            background-color: #343438;
                            color: white;
                            border-radius: 4px;
                            width: 80%;

                        }
                    </style>
                    <div class="name" >
                        <span class="label label-default rank-label samim">{{$user->name}}</span>
                    </div>

                    <div class="profile-header-container">
                        <div class="profile-header-img">
                            <img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" height="300"
                                 width="300"/>
                            <small id="fileHelp" class="form-text text-muted">این عکس نمایه کاربری شماست که کاربران دیگر
                                مشاهده
                                میکنند.
                            </small>
                            <!-- badge -->
                            <div class="rank-label-container">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row justify-content-center text-center">
                    <form action="/profile" method="post" enctype="multipart/form-data">
                        @csrf
                        <style>

                            /****** CODE ******/

                            .file-upload{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
                            .file-upload .file-select{display:block;border: 2px solid #dce4ec;border-radius: 4px;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
                            .file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
                            .file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
                            .file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                            .file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                            .file-upload.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                            .file-upload.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
                            .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
                            .file-upload .file-select.file-select-disabled{opacity:0.65;}
                            .file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
                            .file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
                            .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
                        </style>

                        <div class="file-upload mt-3 samim" style="direction: ltr">
                            <div class="file-select">
                                <div class="file-select-button" id="fileName">انتخاب فایل</div>
                                <div class="file-select-name" id="noFile">هیچ فایلی انتخاب نشده است</div>
                                <input type="file" name="avatar" aria-describedby="fileHelp" id="avatarFile">
                            </div>
                        </div>
                        <script>
                            $('#avatarFile').bind('change', function () {
                                var filename = $("#avatarFile").val();
                                if (/^\s*$/.test(filename)) {
                                    $(".file-upload").removeClass('active');
                                    $("#noFile").text("فایلی انتخاب نشده...");
                                }
                                else {
                                    $(".file-upload").addClass('active');
                                    $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
                                }
                            });
                        </script>

                        <button type="submit" class="btn btn-primary samim mt-3">انجام تغییرات</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection