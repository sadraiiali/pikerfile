@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header beshkaste justify-content-center text-center" style="font-size: 3rem">
                        <img src="/storage/avatars/sad.png" height="100" width="100" alt="">
                        <h1 style="font-size: 4rem">ای بابا ...</h1>
                    </div>

                    <div class="card-body samim text-center">
                        اونی که میخوای نیست :(
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
