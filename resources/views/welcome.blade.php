<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <title>PikerFile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--Bootstrap-->
    <script src="https://kit.fontawesome.com/f72adb371d.js"></script>
    <link rel="stylesheet" href="{{asset('css/font.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>
<body>

<div class="container-fluid text-white" id="header">
    <div class="logo flicker-1">
        <i class="fas fa-cloud-upload-alt"></i>
        <span class="logo-text">پایکر فایل</span>
    </div>
    @if (Route::has('login'))
        <div class="myuploads bounce-top">
            @auth
                <a href="{{ route('home') }}" class="btn btn-primary loginbtn">فایل‌های من</a>
            @else
                <a href="{{ route('login') }}" class="btn btn-primary loginbtn">ورود</a>
                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="btn btn-primary loginbtn">ثبت‌نام</a>
                @endif
            @endauth
        </div>
    @endif

</div>
<div class="container-fluid w-100 h-100 text-white" id="uploadNow">
    <div class="row">
        <div class="center">
            <h1>فایلتان را اینجا بکشید</h1><br>
            <h2>یا</h2><br>
            <button class="btn btn-primary" onclick="document.getElementById('input_file').click()"><i
                        class="far fa-file-alt"></i>
                انتخاب کنید
            </button>
            <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <input type="file" name="cover" aria-describedby="fileHelp" class="d-none" id="input_file"
                       onchange="document.getElementById('submitbtn').click()"></input>

                <button type="submit" class="d-none" id="submitbtn"></button>
            </form>

        </div>
    </div>
</div>
<div class="container-fluid text-white" id="footer">
    <div class="footer-container">
        <ul class="footer-list">
            <li>
                <a href="">گزارش خطا</a>
            </li>
            <li>
                <a href="">قوانین</a>
            </li>
            <li>
                <a href="">درباره‌ما</a>
            </li>
        </ul>
    </div>
</div>

</body>
</html>
